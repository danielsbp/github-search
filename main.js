searchButton = document.querySelector("#searchButton");
searchInput = document.querySelector("#search");

searchComponents = document.querySelector("#searchComponents")

requestType = 0;

searchButton.addEventListener("click", function() {

	username = searchInput.value;
	link = "https://api.github.com/users/"+username.toLowerCase();
	if(username.length == 0){
		//Mostrar mensagem de erro.
		console.log("Deu certo!")
	}
	else {	
		console.log("Dahora")
		xhttp.open("GET", link, true);
		xhttp.send();
	}
});

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    	var resposta = xhttp.responseText;
    	var respostaJSON = JSON.parse(resposta)
    	if (requestType == 0) {
    		removeSearchComponents();
    		renderDescription(respostaJSON);
    	}
    	else if(requestType == 1) {
    		renderRepo(respostaJSON)
    	}

    }
    else if(this.readyState == 4 && this.status == 404) {
    	//Mostrar algo como "usuário não encontrado."
    	var adviceTag = document.createElement('div')
    	adviceTag.setAttribute('class', 'advice')

    	var text = document.createTextNode('Usuário não encontrado!')

    	adviceTag.appendChild(text)
    	searchComponents.appendChild(adviceTag)

    	searchInput.addEventListener('focus', function() {
    		searchComponents.removeChild(adviceTag)
    	})
    }
}

function removeSearchComponents() {
	searchComponents.innerHTML = ''
}

function renderDescription(user) {
	var userImage = document.createElement("img");
	userImage.setAttribute('src', user.avatar_url)

	searchComponents.appendChild(userImage)

	var infoTable = document.createElement("table");
	
	var numberOfTrTags = 4

	var tdText = [['Nome: ', user.name], ['Bio: ', user.bio], ['Seguidores: ', user.followers], ['Quantidade de Repositórios: ', user.public_repos]]
	
	var trElements = []
	var tdElements = []

	for(x = 0; x < numberOfTrTags; x++) {
		var trTag = document.createElement("tr");
		
		console.log('Linha')
		for(y = 0; y < 2; y++) {
			var tdTag = document.createElement("td");
			var text = document.createTextNode(tdText[x][y]);
			tdTag.appendChild(text)
			trTag.appendChild(tdTag)
		}
		infoTable.appendChild(trTag)

	}
	searchComponents.appendChild(infoTable)

	var backButton = document.createElement("button")
	var text = document.createTextNode("Voltar")
	backButton.appendChild(text)

	backButton.addEventListener("click", function() {
		location.reload();
	})

	backButton.setAttribute('class', 'center button')

	searchComponents.appendChild(backButton);

	var aLinkRep = document.createElement("div");
	aLinkRep.setAttribute('style', 'text-align: center; cursor: pointer;');

	var text = document.createTextNode("Veja os repositórios de "+user.name)
	
	aLinkRep.appendChild(text)

	aLinkRep.addEventListener('click', () => {
		requestType = 1;
		xhttp.open("GET", user.repos_url, true);
		xhttp.send();
	});

	searchComponents.appendChild(aLinkRep)
}

function renderRepo(repos) {
	console.log(repos)
}
